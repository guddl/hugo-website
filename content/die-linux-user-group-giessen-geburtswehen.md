+++
title =  "Die Linux User Group Giessen - Geburtswehen"
date = 1996-08-15
draft = false
comment = false
toc = true
reward = true
categories = [
  "Linux"
]
tags = [
  "LUGG"
]
series = [
  ""
]
images = [
  "images/lugg-logo.png"
]

+++

Es ist nie zu spät eine Linux User Group zu gründen. Wir werfen hier einen kurzen Blick auf die Entstehungsgeschichte der LUG Gießen.

Linux ist ein sehr gutes und leistungsfähiges Betriebssystem, wie wir wissen. Hat man es einmal installiert – und es läuft stabil, fühlt man sich schon wie jemand Besonderes. Dann probiert man dies und jenes aus. Doch mit der Zeit möchte man auch andere an seiner Freude über dieses Betriebssystem teilhaben lassen. Denn immer nur alleine im stillen Kämmerlein zu sitzen und arbeiten, ist auch nicht immer das Wahre. Da kam mir Anfang diesen Jahres die Idee, eine weitere Linux User Group in meiner Nähe zu gründen. Nach einigem Überlegen, ob sich das überhaupt rentiert, oder wie ich das ganze anpacken soll, machte ich mir erst einmal keine Gedanken über dieses Vorhaben. Doch ich kam einfach nicht davon ab. So startete ich am 03.02. dieses Jahres eine Anfrage in der S.u.S.E.-Mailingliste (http://www.suse.de/) und in einigen regionalen und nationalen Newsgroups. Damit war der erste Schritt getan und nicht mehr rückgängig zu machen. Nun wartete ich einfach ab. Und nach einigen Tagen hatten sich auch ein paar Linuxer gemeldet. Es waren acht an der Zahl. Nun ging es an die Festlegung eines ersten Termins und auf die Suche nach einer geeigneten Kneipe zur Austragung. Die Kneipe war zwar schnell gefunden – doch der Termin …~ Es ließen sich nicht alle unter einen Hut bekommen. Somit legte ich, nach heftiger Diskussion, einfach mal als ersten Termin den 26.02. 19:00 Uhr fest.

### Das erste Mal
26.02.98 – Es war Aschermittwoch. Ich machte mich also auf den Weg nach Gießen. Als ich an der Kneipe ankam, sagte man mir, daß sie heute geschlossen bleibt, da sie es noch nicht geschafft hätten, das Chaos vom Vortag zu beseitigen. So stand ich mir vor der Kneipe die Beine in den Bauch und wartete auf weitere „Erst-Stammtisch-Teilnehmer“. Um 19:20 Uhr trafen dann auch noch zwei weitere Mohikaner ein. Wir hängten kurzer Hand einen Zettel mit dem neuen Treffpunkt an die Kneipentür und gingen in eine Nachbarkneipe. Ein gemütlicher Abend begann, und alle Ideen und Vorschläge für die Zukunft hörten sich recht vielversprechend an – obwohl der erste Abend mit nur drei Linuxern über die Bühne ging. Die weiteren drei Abende, die in einem Abstand von zwei Wochen stattfanden, waren dann nur noch von mir und einem weiterem Linuxer besucht. Wir waren zugegebenermaßen etwas frustriert.

### Jetzt gilt’s!
Am vierten Abend hatten wir es satt. Wir starteten eine Offensive. Jetzt oder nie. Es wurden Plakate entworfen, weiterhin Werbung in Mailinglisten, Newsgroups, Zeitungen an der Uni und FH-Gießen gemacht. Auch eine erste kleine Website entstand. Von einer Gießener Buchhandlung erhielten wir Unterstßtzung, und dort hängten wir dann auch unser erstes Plakat auf. Auf Wunsch des Inhabers in Größe DIN A3 ~~:-) Jetzt ging es mit der LUG-Gießen steil bergauf. Im Handumdrehen waren wir ca. zehn begeisterte Linux User, die sich für dieses Jahr noch einiges vorgenommen haben.

### Was ist – und was soll noch passieren?
Nach fast einem halben Jahr sind wir jetzt ein fester Kreis von ca. zehn bis zwölf Personen pro Treffen. Damit läßt sich doch schon einiges anfangen. Über eine Mailingliste kann man interne Probleme diskutieren, und der eigene Server der LUGG ließ auch nicht lange auf sich warten. Daß es so schnell ging, haben wir zwei treuen Mitgliedern zu verdanken (Hallo Marc, hallo Wilhelm). Der eine nervte seinen Chef an der UNI und der andere spendete seinen Zweitrechner. Damit die Treffen nicht nur zum Genießen des Äppelwois (hochdeutsch: Apfelwein) und zum Austausch eigener Erlebnisse mit Linux wird, findet jetzt an jedem zweiten Treffen ein Themenabend statt. Jeder, der sich für ein bestimmtes Thema interessiert und Lust hat, hält dazu einen kleinen Vortrag. Auch ein paar Projekte können jetzt in Angriff genommen werden. Was dieses Jahr vielleicht noch organisiert werden soll, ist eine Installationsparty. Doch darum konnten wir uns bis jetzt noch nicht kümmern, da es erst einige andere Dinge zu erledigen gab und gibt.

### Fazit
Ich kann jedem, der sich ‚alleine‘ fühlt, und keine LUG in seiner Nähe hat, ermutigen, sich daran zu machen, mal eine eMail an Mailinglisten und Newsgroups zu schicken, um eine User-Group zu gründen. In Gießen war dies ein voller Erfolg!

Autor: Andreas Gutowski

Dieser Artikel erschien in der August-Ausgabe 1998 des Linux Magazins.

Die Linux User Group Gießen
